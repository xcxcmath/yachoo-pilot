import _ from "lodash";
import { Room, Client, Delayed } from "colyseus";
import {
  ClientMessageType,
  isClientToggleDiceData,
  isClientPostCategoryData,
} from "@shared/messages/ClientMessage";
import {
  ServerMessageType,
  ServerShowStringEffectMessage,
  ServerShowVisualEffectMessage,
  ServerShowResultMessage,
  ServerShowCategoryHighlightMessage,
  ServerMessage,
} from "@shared/messages/ServerMessage";
import { Category } from "@shared/models/Category";
import { GameState } from "./models/GameState";
import { RuleType } from "@shared/models/Rules";
import { maxNameLength } from "@shared/constants";

import { categoryLongName } from "@shared/helpers";

const MAX_ROLL = 3;
const MAX_CLIENT = 4;

export interface GameOption {
  ruleType: RuleType;
}

export class GameRoom extends Room<GameState> {
  maxClients = MAX_CLIENT;

  public rollingTimeout: Delayed | undefined;
  rollingTimeoutDruation = 1500;

  constructor() {
    super();

    this.broadcastShowMessageAfterRolled = this.broadcastShowMessageAfterRolled.bind(
      this
    );
    this.broadcastShowMessageAfterFinished = this.broadcastShowMessageAfterFinished.bind(
      this
    );
    this.broadcastShowHighlight = this.broadcastShowHighlight.bind(this);
    this.broadcastServerMessage = this.broadcastServerMessage.bind(this);
  }

  onCreate(options: GameOption) {
    this.setState(
      new GameState(
        options.ruleType,
        this.maxClients,
        MAX_ROLL,
        this.broadcastShowMessageAfterFinished
      )
    );

    this.onMessage(ClientMessageType.TOGGLE_READY, (client, data) => {
      if (
        !this.state.rule.stateChecker.isPlayerReadyToggleable(
          this.state,
          client.sessionId
        )
      )
        return;

      const player = this.state.players.get(client.sessionId);
      if (player === undefined) return;

      player.ready = !player.ready;

      if (this.state.rule.stateChecker.isStartable(this.state)) {
        this.state.startGame();
      }
    });

    this.onMessage(ClientMessageType.TOGGLE_DICE, (client, data) => {
      if (
        !this.state.rule.stateChecker.isPlayerDiceToggleable(
          this.state,
          client.sessionId
        )
      )
        return;

      if (!isClientToggleDiceData(data)) return;
      const { index } = data;

      const fixedPos = this.state.dices.diceFixed.indexOf(index);
      const rolledPos = this.state.dices.diceRolled.indexOf(index);
      const fixedFound = fixedPos >= 0;
      const rolledFound = rolledPos >= 0;

      if ((fixedFound && rolledFound) || (!fixedFound && !rolledFound)) {
        return;
      }
      if (fixedFound) {
        // fixed -> rolled
        this.state.dices.diceFixed.splice(fixedPos, 1);
        this.state.dices.diceRolled.push(index);
      } else {
        // rolled -> fixed
        this.state.dices.diceRolled.splice(rolledPos, 1);
        this.state.dices.diceFixed.push(index);
      }
    });

    this.onMessage(ClientMessageType.ROLL, (client, data) => {
      if (
        !this.state.rule.stateChecker.isPlayerRollable(
          this.state,
          client.sessionId
        )
      )
        return;

      this.state.rolling = true;
      this.state.rollCount--;
      this.rollingTimeout = this.clock.setTimeout(() => {
        this.state.dices.generateRolled();
        this.state.rolling = false;

        this.broadcastShowMessageAfterRolled();
      }, this.rollingTimeoutDruation);
    });

    this.onMessage(ClientMessageType.POST_CATEGORY, (client, data) => {
      if (
        !this.state.rule.stateChecker.isPlayerPostable(
          this.state,
          client.sessionId
        )
      )
        return;

      if (!isClientPostCategoryData(data)) return;
      const { category } = data;
      const player = this.state.players.get(client.sessionId);
      if (player === undefined) return;

      const result = this.state.rule.scoreCalculator.postCategory(
        player.scores,
        category,
        this.state.dices.diceNum
      );

      if (result !== undefined) {
        this.broadcastShowHighlight(client.sessionId, category);
        this.state.nextTurnOrFinish();
      }

      if (!this.state.proceed) {
        this.rollingTimeout?.clear();
      }
    });
  }

  onAuth(client: Client, options: any, _: any) {
    const { name } = options as { name?: string };
    if (
      typeof name === "string" &&
      name.length > 0 &&
      name.length <= maxNameLength
    ) {
      return true;
    }
    return false;
  }

  onJoin(client: Client, options: any, auth: any) {
    this.state.addUser(options.name, client.sessionId);
  }

  onLeave(client: Client, consented: boolean) {
    this.state.deleteUser(client.sessionId);
    if (!this.state.proceed) {
      this.rollingTimeout?.clear();
    }
  }

  broadcastServerMessage(msg: ServerMessage) {
    this.broadcast(msg.message, msg.data);
  }

  broadcastShowMessageAfterRolled() {
    const scores = this.state.players.get(
      this.state.playerOrder[this.state.turn]
    )?.scores;
    if (scores === undefined) return;
    const showTemp: (
      list: Category[]
    ) => { catName: string; score: number | undefined } | undefined = (
      list: Category[]
    ) =>
      _.chain(list)
        .map((cat) => {
          return {
            catName: categoryLongName(cat),
            score: this.state.rule.scoreCalculator.calcCategoryScore(
              scores,
              cat,
              this.state.dices.diceNum
            ),
          };
        })
        .maxBy(({ score }) => score)
        .value();

    const stringShowTemp = showTemp(this.state.rule.stringShowCategories);
    const visualShowTemp = showTemp(this.state.rule.visualShowCategories);

    if (
      stringShowTemp !== undefined &&
      stringShowTemp.score !== undefined &&
      stringShowTemp.score > 0
    ) {
      const { catName } = stringShowTemp;
      const msg: ServerShowStringEffectMessage = {
        message: ServerMessageType.SHOW_STRING_EFFECT,
        data: { str: catName },
      };
      this.broadcastServerMessage(msg);
    }

    if (
      visualShowTemp !== undefined &&
      visualShowTemp.score !== undefined &&
      visualShowTemp.score > 0
    ) {
      const msg: ServerShowVisualEffectMessage = {
        message: ServerMessageType.SHOW_VISUAL_EFFECT,
        data: {},
      };
      this.broadcastServerMessage(msg);
    }
  }

  broadcastShowMessageAfterFinished() {
    const data = this.state.playerOrder
      .map((sessionId) => ({
        sessionId,
        player: this.state.players.get(sessionId),
      }))
      .filter(({ player }) => player !== undefined)
      .filter(({ player }) =>
        this.state.rule.scoreCalculator.isFinished(player!.scores)
      )
      .map(({ sessionId, player }) => ({
        sessionId,
        name: player!.name,
        score: this.state.rule.scoreCalculator.getTotalScore(player!.scores),
      }));

    if (data.length === 0) return;

    const msg: ServerShowResultMessage = {
      message: ServerMessageType.SHOW_RESULT,
      data: {
        sessionIDs: data.map((it) => it.sessionId),
        scores: data.map((it) => it.score),
        names: data.map((it) => it.name),
      },
    };

    this.broadcastServerMessage(msg);
  }

  broadcastShowHighlight(sessionId: string, category: Category) {
    const msg: ServerShowCategoryHighlightMessage = {
      message: ServerMessageType.SHOW_CATEGORY_HIGHLIGHT,
      data: {
        sessionId,
        category,
      },
    };

    this.broadcastServerMessage(msg);
  }
}
