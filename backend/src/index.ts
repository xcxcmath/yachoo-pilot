import express from "express";
import { Server } from "colyseus";
import { createServer } from "http";
import { GameRoom } from "./GameRoom";
import { monitor } from "@colyseus/monitor";

const port = Number(process.env.port) || 3001;

const app = express();

const gameServer = new Server({
  server: createServer(app),
});

gameServer.define("default", GameRoom, { ruleType: "default" });

app.use("/colyseus", monitor());
gameServer.listen(port);

console.log(`Listening on ws://localhost:${port}`);
