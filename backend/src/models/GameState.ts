import _ from "lodash";
import { Schema, type, MapSchema, ArraySchema } from "@colyseus/schema";
import { IGameStateProp } from "@shared/models/GameState";
import { RuleType, RuleHelper, ruleMap } from "@shared/models/Rules";
import { User } from "./User";
import { Dices } from "./Dices";

export class GameState extends Schema implements IGameStateProp {
  @type({ map: User }) players: MapSchema<User>;
  @type({ map: User }) watchers: MapSchema<User>;
  @type("uint32") maxPlayers: number;
  @type("string") ruleType: RuleType;

  @type("boolean") proceed: boolean;
  @type(["string"]) playerOrder: ArraySchema<string>;
  @type(["string"]) watcherOrder: ArraySchema<string>;
  @type("int32") turn: number;

  @type("uint8") rollCount: number;
  @type("uint8") maxRollCount: number;

  @type(Dices) dices: Dices;

  @type("boolean") rolling: boolean;

  rule: RuleHelper;
  beforeFinish: () => void;

  constructor(
    ruleType: RuleType,
    maxPlayers: number,
    maxRollCount: number,
    beforeFinish: () => void
  ) {
    super();
    this.players = new MapSchema<User>();
    this.watchers = new MapSchema<User>();
    this.maxPlayers = maxPlayers;
    this.ruleType = ruleType;

    this.proceed = false;
    this.playerOrder = new ArraySchema<string>();
    this.watcherOrder = new ArraySchema<string>();
    this.turn = 0;

    this.rollCount = maxRollCount;
    this.maxRollCount = maxRollCount;

    this.dices = new Dices();

    this.rolling = false;

    this.rule = ruleMap[this.ruleType];
    this.beforeFinish = beforeFinish;
  }

  addUser(name: string, sessionId: string) {
    const newUser = new User(name, sessionId);
    if (this.proceed) {
      this.watchers.set(sessionId, newUser);
      this.watcherOrder.push(sessionId);
    } else {
      this.players.set(sessionId, newUser);
      this.playerOrder.push(sessionId);
    }
  }

  deleteUser(sessionId: string) {
    if (this.players.has(sessionId)) {
      this.players.delete(sessionId);
    }
    if (this.watchers.has(sessionId)) {
      this.watchers.delete(sessionId);
    }

    const playerIdx = this.playerOrder.indexOf(sessionId);
    const watcherIdx = this.watcherOrder.indexOf(sessionId);

    if (watcherIdx >= 0) {
      this.watcherOrder.splice(watcherIdx, 1);
    }
    if (playerIdx >= 0) {
      this.playerOrder.splice(playerIdx, 1);
      if (this.proceed) {
        if (this.playerOrder.length < 2) {
          this.finishGame();
        } else if (this.turn === playerIdx) {
          this.nextTurnOrFinish(0);
        } else if (this.turn > playerIdx) {
          --this.turn;
        }
      }
    }
  }

  startGame() {
    if (this.players.size <= 0) return;

    this.players.forEach((user) => user.clear());
    this.proceed = true;
    this.rollCount = this.maxRollCount;
    this.turn = 0;
    this.rolling = false;
    this.dices.clear();
  }

  finishGame() {
    this.proceed = false;
    this.dices.clear();

    this.beforeFinish();

    this.watcherOrder.clear();
    this.watchers.forEach((user) => {
      this.players.set(user.sessionId, user);
      this.playerOrder.push(user.sessionId);
    });
    this.watchers.clear();

    this.players.forEach((user) => {
      user.ready = false;
    });
    this.rolling = false;

    const copiedOrder = [...this.playerOrder];
    const shuffledOrder = _.shuffle(copiedOrder);
    this.playerOrder.clear();
    this.playerOrder.push(...shuffledOrder);
  }

  nextTurnOrFinish(increment: 0 | 1 = 1) {
    this.rollCount = this.maxRollCount;

    let nextIdx = this.turn + increment;
    if (nextIdx >= this.players.size) {
      nextIdx = 0;
    }
    do {
      const thePlayer = this.players.get(this.playerOrder[nextIdx]);
      if (
        thePlayer !== undefined &&
        !this.rule.scoreCalculator.isFinished(thePlayer.scores)
      ) {
        break;
      }

      ++nextIdx;
      if (nextIdx >= this.players.size) {
        nextIdx = 0;
      }
    } while (this.turn !== nextIdx);

    const thePlayer = this.players.get(this.playerOrder[nextIdx]);
    if (
      thePlayer !== undefined &&
      !this.rule.scoreCalculator.isFinished(thePlayer.scores)
    ) {
      this.turn = nextIdx;
      this.dices.clear();
    } else {
      this.finishGame();
    }
  }
}
