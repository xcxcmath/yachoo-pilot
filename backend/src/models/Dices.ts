import { IDicesProp } from "@shared/models/Dices";
import { randomIntInclusive } from "@shared/helpers/index";
import { Schema, type, ArraySchema } from "@colyseus/schema";

export class Dices extends Schema implements IDicesProp {
  @type(["number"]) diceNum: ArraySchema<number>;
  @type(["number"]) diceFixed: ArraySchema<number>;
  @type(["number"]) diceRolled: ArraySchema<number>;

  generateRolled() {
    this.diceRolled.forEach((index) => {
      this.diceNum[index] = randomIntInclusive(1, 6);
    });
  }

  constructor() {
    super();
    this.diceNum = new ArraySchema(1, 1, 1, 1, 1);
    this.diceFixed = new ArraySchema<number>();
    this.diceRolled = new ArraySchema<number>(0, 1, 2, 3, 4);
    this.generateRolled();
  }

  clear() {
    this.diceFixed.clear();
    this.diceRolled.clear();
    this.diceRolled.push(0, 1, 2, 3, 4);
    this.generateRolled();
  }
}
