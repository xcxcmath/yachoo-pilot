import { Schema, type, MapSchema } from "@colyseus/schema";
import { IUserProp } from "@shared/models/User";

export class User extends Schema implements IUserProp {
  @type("string") name: string;
  @type("string") sessionId: string;
  @type("boolean") ready: boolean;
  @type({ map: "number" }) scores: MapSchema<number>;

  constructor(name: string, sessionId: string) {
    super();
    this.name = name;
    this.sessionId = sessionId;
    this.ready = false;
    this.scores = new MapSchema<number>();
  }

  clear() {
    this.ready = false;
    this.scores.clear();
  }
}
