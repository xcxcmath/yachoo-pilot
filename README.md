# Yachoo Pilot Project

This project is a pilot; really easy to be changed dramatically.

## Components

- `backend` : Colyseus server
- `frontend` : React front-end
- `shared` : Common structures and functions for `backend` and `frontend`
- `nginx` : Proxy
  - pass `/` and `/sockjs-node` to `frontend` with port `3000`
  - pass `/ws` to `backend` with port `3001`, removing `/ws`

## How to Run in Dev. Mode

Using `docker-compose` is highly recommended, as it utilizes proxy `nginx`.

1. _(Optional)_ `docker-compose build` : builds docker images from `Dockerfile`s. You may check everything is OK.
2. `docker-compose up` : initiates server containers.
3. `docker-compose down` : removes the containers.

Alternatively, you can run them manually, which don't use proxy.

1. Run `yarn install` from `backend`, `frontend`, and `shared`.
2. Run `yarn run dev` from `backend`.
3. Run `yarn run start` from `frontend`.

For `frontend`, an environment variable `REACT_APP_PROXY_USED` is set in `docker-compose.yml`, which
makes to use proxy. If manually executed, the variable should not be set.

## How to Run in Prod. Mode

Not tested yet.
