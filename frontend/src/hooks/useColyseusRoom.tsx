import colyseusRoom, { RoomStore } from "../states/colyseus-room";
import { useContext } from "react";

const useColyseusRoom = <T extends unknown>(f: (store: RoomStore) => T) => {
  const store = useContext(colyseusRoom);
  return f(store);
};

export default useColyseusRoom;
