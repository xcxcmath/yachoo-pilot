import "./Messages.scss";
import React from "react";
import { observer } from "mobx-react-lite";
import { motion } from "framer-motion";

import useColyseusRoom from "../hooks/useColyseusRoom";
import { messageDuration } from "../config";

const BottomMessages: React.FC = () => {
  const messageQueue = useColyseusRoom((store) => store.messageQueue);
  const queueReversed = [...messageQueue.queue].reverse();
  return (
    <div className="bottom-messages">
      {queueReversed.map((elem) => {
        const { body, color, header, size, key } = elem;
        const className = [
          "bottom-message",
          "message",
          ...(color !== undefined ? [color] : []),
          ...(size !== undefined ? [size] : []),
        ].join(" ");

        return (
          <motion.div
            animate={{
              opacity: [1, 1, 0],
            }}
            transition={{
              duration: messageDuration / 1000,
              ease: "easeIn",
            }}
            className={className}
            key={key}
          >
            {header && (
              <div className="message-header">
                <p>{header}</p>
              </div>
            )}
            <div className="message-body">{body}</div>
          </motion.div>
        );
      })}
    </div>
  );
};

export default observer(BottomMessages);
