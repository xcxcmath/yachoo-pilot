import "./DiceBoard.scss";
import React from "react";
import { observer } from "mobx-react-lite";
import { motion } from "framer-motion";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDice } from "@fortawesome/free-solid-svg-icons";

import StringEffect from "./StringEffect";
import { ClientMessageType } from "@shared/messages/ClientMessage";
import { ruleMap } from "@shared/models/Rules";
import useColyseusRoom from "../hooks/useColyseusRoom";
import ErrorBox from "../components/error-box/ErrorBox";
import { Dice } from "../components/dice";

const DiceBoard: React.FC = () => {
  const state = useColyseusRoom((store) => store.state);
  const sendClientMessage = useColyseusRoom((store) => store.sendClientMessage);
  const sessionId = useColyseusRoom((store) => store.roomProp?.sessionId);
  if (state === undefined || sessionId === undefined) {
    return <ErrorBox />;
  }
  const {
    ruleType,
    rollCount,
    maxRollCount,
    rolling,
    dices,
    proceed,
    players,
  } = state;
  const { diceNum, diceFixed, diceRolled } = dices;
  const rule = ruleMap[ruleType];
  if (!proceed) {
    return (
      <div className="dice-board-container box">
        <div
          className="button is-large center-button"
          onClick={() =>
            sendClientMessage({
              message: ClientMessageType.TOGGLE_READY,
              data: {},
            })
          }
        >
          {players.get(sessionId)?.ready ? "Unready" : "Ready"}
        </div>
      </div>
    );
  }
  const {
    isIdle,
    isPlayerDiceToggleable,
    isPlayerRollable,
  } = rule.stateChecker;
  const idle = isIdle(state);
  const diceToggleable = isPlayerDiceToggleable(state, sessionId);
  const rollable = isPlayerRollable(state, sessionId);

  const isInFixed = (idx: number) => diceFixed.indexOf(idx) >= 0;
  const handleDiceClick = (idx: number) => () => {
    if (diceToggleable) {
      sendClientMessage({
        message: ClientMessageType.TOGGLE_DICE,
        data: { index: idx },
      });
    }
  };
  const indices = [...Array(5).keys()];
  const types = indices.map((idx) => (isInFixed(idx) ? "fixed" : "rolled"));
  const typeIndices = indices
    .map((idx) => diceFixed.indexOf(idx))
    .map((typeIdx, idx) => (typeIdx >= 0 ? typeIdx : diceRolled.indexOf(idx)));
  const values = indices.map((idx) => diceNum[idx]);
  return (
    <div className="dice-board-container box">
      <StringEffect />
      <motion.div className="roll-count-box box">
        <span className="is-large">
          <FontAwesomeIcon icon={faDice} className="fa-2x" />
        </span>
        <span className="is-large is-size-4">{`${rollCount} / ${maxRollCount}`}</span>
        <span>
          <button
            className="roll-button button is-large"
            disabled={!rollable}
            onClick={() => {
              sendClientMessage({
                message: ClientMessageType.ROLL,
                data: {},
              });
            }}
          >
            Roll
          </button>
        </span>
      </motion.div>
      {indices.map((idx) => (
        <Dice
          key={`dice-${idx}`}
          idle={idle}
          rolling={rolling}
          type={types[idx]}
          typeIndex={typeIndices[idx]}
          value={values[idx]}
          onClick={handleDiceClick(idx)}
        />
      ))}
    </div>
  );
};

export default observer(DiceBoard);
