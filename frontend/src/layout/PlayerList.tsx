import "./PlayerList.scss";
import React from "react";
import { observer } from "mobx-react-lite";
import _ from "lodash";

import { ruleMap } from "@shared/models/Rules";
import useColyseusRoom from "../hooks/useColyseusRoom";
import { ErrorBox } from "../components/error-box";
import { UserList, UserListCell } from "../components/user-list";

const PlayerList: React.FC = () => {
  const state = useColyseusRoom((store) => store.state);
  const sessionId = useColyseusRoom((store) => store.roomProp?.sessionId);
  if (state === undefined || sessionId === undefined) {
    return <ErrorBox />;
  }
  const { ruleType, players, playerOrder, proceed } = state;

  const rule = ruleMap[ruleType];
  const isPlayersTurn = (id: string) =>
    rule.stateChecker.isPlayersTurn(state, id);
  const playerList = _.chain(playerOrder)
    .map((id) => players.get(id))
    .compact()
    .value();
  const maxScore = _.chain(playerList)
    .map((it) => rule.scoreCalculator.getTotalScore(it.scores))
    .max()
    .value();

  return (
    <div className="user-list block">
      <UserList heading="Players" primary>
        {playerList.map((player) => (
          <UserListCell
            active={isPlayersTurn(player.sessionId)}
            name={player.name}
            isYou={player.sessionId === sessionId}
            isLeader={
              proceed &&
              maxScore === rule.scoreCalculator.getTotalScore(player.scores)
            }
            isReady={player.ready}
            key={`player-list-${player.sessionId}`}
          />
        ))}
      </UserList>
    </div>
  );
};

export default observer(PlayerList);
