import "./GamePage.scss";
import React, { useState } from "react";
import { observer } from "mobx-react-lite";
import _ from "lodash";
import Confetti from "react-confetti";
import { useWindowSize } from "react-use";

import ScoreTable from "./ScoreTable";
import PlayerList from "./PlayerList";
import ObserverList from "./ObserverList";
import DiceBoard from "./DiceBoard";
import { GamePageHeader } from "../components/game-page-header";
import { AlertModal } from "../components/alert-modal";
import { ResultModal } from "../components/result-modal";
import useColyseusRoom from "../hooks/useColyseusRoom";

const GamePage: React.FC = () => {
  const { width, height } = useWindowSize();

  const [isModalActive, setIsModalActive] = useState(false);
  const proceed = useColyseusRoom((store) => store.state?.proceed ?? false);
  const disconnect = useColyseusRoom((store) => store.disconnect);
  const sessionId = useColyseusRoom((store) => store.roomProp?.sessionId);
  const {
    result,
    deactivateResult,
    visualEffect,
    deactivateVisualEffect,
  } = useColyseusRoom((store) => store);
  const maxScore = _.max(result?.scores);
  const selfIndex =
    sessionId !== undefined ? result?.sessionIDs.indexOf(sessionId) : undefined;
  const selfScore =
    selfIndex !== undefined ? result?.scores[selfIndex] : undefined;
  const showConfetti =
    result !== undefined && maxScore !== undefined && selfScore === maxScore;

  return (
    <>
      <AlertModal
        isActive={isModalActive}
        onConfirm={() => {
          setIsModalActive(false);
          disconnect();
        }}
        onCancel={() => {
          setIsModalActive(false);
        }}
      >
        <p className="is-size-4">Do you really want to leave?</p>
      </AlertModal>
      <ResultModal
        isActive={result !== undefined}
        showConfetti={showConfetti}
        onClose={deactivateResult}
      >
        <div className="panel">
          <p className="panel-heading has-text-centered">Result</p>
          {result !== undefined && (
            <nav className="level">
              {result.sessionIDs.map((id, index) => (
                <div
                  className="level-item has-text-centered"
                  key={`result-modal-${id}`}
                >
                  <div>
                    <p className="heading">
                      {result.names[index]}{" "}
                      {id === sessionId && (
                        <span className="tag is-primary">You</span>
                      )}
                    </p>
                    <p
                      className={`title${
                        maxScore === result.scores[index]
                          ? " has-text-success"
                          : ""
                      }`}
                    >
                      {result.scores[index]}
                    </p>
                  </div>
                </div>
              ))}
            </nav>
          )}
        </div>
      </ResultModal>
      {visualEffect && (
        <Confetti
          width={width}
          height={height}
          opacity={0.7}
          numberOfPieces={250}
          recycle={false}
          onConfettiComplete={deactivateVisualEffect}
        />
      )}
      <GamePageHeader
        isPrimary={proceed}
        onLeaveClick={() => setIsModalActive(true)}
      />
      <div className="game-contents">
        <ScoreTable />
        <DiceBoard />
        <div className="user-lists">
          <PlayerList />
          <ObserverList />
        </div>
      </div>
    </>
  );
};

export default observer(GamePage);
