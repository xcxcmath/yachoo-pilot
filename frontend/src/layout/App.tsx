import "./App.scss";
import React from "react";
import { observer } from "mobx-react-lite";

import useColyseusRoom from "../hooks/useColyseusRoom";
import LoginBox from "./LoginBox";
import GamePage from "./GamePage";
import BottomMessages from "./Messages";

const App: React.FC = () => {
  const joinStatus = useColyseusRoom((store) => store.joinStatus);

  const success = joinStatus === "success";
  const mainPageIndex = "1";
  const leftPage = "0";
  const rightPage = "2";

  return (
    <div id="App">
      <div
        className={`app-page app-page-${!success ? mainPageIndex : leftPage}`}
      >
        <LoginBox />
      </div>
      <div
        className={`app-page app-page-${success ? mainPageIndex : rightPage}`}
      >
        <GamePage />
      </div>
      <BottomMessages />
    </div>
  );
};

export default observer(App);
