import "./StringEffect.scss";
import React from "react";
import { observer } from "mobx-react-lite";
import { motion } from "framer-motion";
import { stringEffectDuration } from "../config";
import useColyseusRoom from "../hooks/useColyseusRoom";
import { conditionalArrayElement as CAE } from "@shared/helpers/index";

const StringEffect: React.FC = () => {
  const { stringEffectQueue } = useColyseusRoom((store) => store);
  const queue = [...stringEffectQueue.queue];
  const className = ["string-effect-modal"].join(" ");
  return (
    <div className={className}>
      {queue.map((str) => (
        <motion.div
          layout
          className="string-effect-modal-elem"
          key={`string-effect-modal-elem-${str}`}
          animate={{
            scale: [1, 3, 3.1, 4],
            opacity: [0, 1, 1, 0],
          }}
          transition={{ duration: stringEffectDuration / 1000 }}
        >
          <h1 className="has-text-weight-semibold">{str}</h1>
        </motion.div>
      ))}
    </div>
  );
};

export default observer(StringEffect);
