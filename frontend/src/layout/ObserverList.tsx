import "./PlayerList.scss"; // same as PlayerList
import React from "react";
import { observer } from "mobx-react-lite";
import _ from "lodash";

import useColyseusRoom from "../hooks/useColyseusRoom";
import { ErrorBox } from "../components/error-box";
import { UserList, UserListCell } from "../components/user-list";

const ObserverList: React.FC = () => {
  const state = useColyseusRoom((store) => store.state);
  const sessionId = useColyseusRoom((store) => store.roomProp?.sessionId);
  if (state === undefined || sessionId === undefined) {
    return <ErrorBox />;
  }
  const { watchers, watcherOrder } = state;
  const watcherList = _.chain(watcherOrder)
    .map((id) => watchers.get(id))
    .compact()
    .value();

  return (
    <div className="user-list block">
      <UserList
        heading="Observers"
        description="Will join as players after this match."
      >
        {watcherList.map((watcher) => (
          <UserListCell
            name={watcher.name}
            isYou={watcher.sessionId === sessionId}
            key={`watcher-list-${watcher.sessionId}`}
          />
        ))}
      </UserList>
    </div>
  );
};

export default observer(ObserverList);
