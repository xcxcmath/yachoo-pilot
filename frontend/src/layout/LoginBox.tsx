import "./LoginBox.scss";
import React from "react";
import { observer } from "mobx-react-lite";
import { LoginForm } from "../components/login-form";
import useColyseusRoom from "../hooks/useColyseusRoom";

const LoginBox: React.FC = () => {
  const joinStatus = useColyseusRoom((store) => store.joinStatus);
  const connect = useColyseusRoom((store) => store.connect);

  const className = ["login-box-container", "box"].join(" ");
  return (
    <div className={className}>
      <LoginForm
        joinStatus={joinStatus}
        onSubmit={(name, ruleType) => connect(name, ruleType)}
      />
    </div>
  );
};

export default observer(LoginBox);
