import "./ScoreTable.scss";
import React from "react";
import { observer } from "mobx-react-lite";
import _ from "lodash";
import useColyseusRoom from "../hooks/useColyseusRoom";

import { ClientMessageType } from "@shared/messages/ClientMessage";
import { ruleMap } from "@shared/models/Rules";
import ErrorBox from "../components/error-box/ErrorBox";
import {
  PlayerNamesRow,
  PlayerScoresRow,
  SubtotalRow,
  TotalRow,
} from "../components/score-table-rows";
import { defaultCategoriesList } from "@shared/models/Category";

const ScoreTable: React.FC = () => {
  const state = useColyseusRoom((store) => store.state);
  const sessionId = useColyseusRoom((store) => store.roomProp?.sessionId);
  const sendClientMessage = useColyseusRoom((store) => store.sendClientMessage);
  const categoryHighlightQueue = useColyseusRoom(
    (store) => store.categoryHighlightQueue
  );
  if (state === undefined || sessionId === undefined) {
    return <ErrorBox />;
  }
  const { ruleType, players, playerOrder, dices } = state;
  const { diceNum } = dices;

  const rule = ruleMap[ruleType];
  const { isPlayersTurn, isPlayerPostable } = rule.stateChecker;
  const {
    calcCategoryScore,
    getTotalScore,
    getEyesCategoriesScore,
    eyesBonusThreshold,
    eyesBonus,
  } = rule.scoreCalculator;
  const playerList = _.chain(playerOrder)
    .map((id) => players.get(id))
    .compact()
    .value();
  const playerScores = defaultCategoriesList.map((cat) => ({
    cat,
    row: playerList.map((player) => {
      const id = player.sessionId;
      const score = player.scores.get(cat);
      const evaluated =
        score === undefined
          ? calcCategoryScore(player.scores, cat, diceNum)
          : undefined;
      const canPostByID = isPlayerPostable(state, id);
      const canPostBySelf = canPostByID && id === sessionId;
      const onPost = canPostBySelf
        ? () => {
            sendClientMessage({
              message: ClientMessageType.POST_CATEGORY,
              data: {
                category: cat,
              },
            });
          }
        : undefined;
      const highlight =
        categoryHighlightQueue.queue.findIndex(
          (data) => data.sessionId === id && data.category === cat
        ) >= 0;
      return {
        id,
        score,
        evaluated,
        canPostByID,
        canPostBySelf,
        onPost,
        highlight,
      };
    }),
  }));
  const playerTotalScore = playerList.map((player) => ({
    id: player.sessionId,
    score: getTotalScore(player.scores),
  }));
  const playerEyesCategoriesScore = playerList.map((player) => ({
    id: player.sessionId,
    score: getEyesCategoriesScore(player.scores),
  }));

  const panelClassName = [
    "score-table-container",
    "panel",
    "has-text-centered",
    ...(isPlayersTurn(state, sessionId) ? ["is-primary"] : []),
  ].join(" ");

  return (
    <div className={panelClassName}>
      <div className="panel-heading">Score Table</div>
      <table className="score-table table is-bordered">
        <thead className="is-size-5">
          <PlayerNamesRow players={playerList} />
        </thead>
        <tbody>
          {playerScores.slice(0, 6).map(({ cat, row }) => (
            <PlayerScoresRow
              category={cat}
              row={row}
              key={`score-table-row-${cat}`}
            />
          ))}
          <SubtotalRow
            subtotalScores={playerEyesCategoriesScore}
            threshold={eyesBonusThreshold}
            bonus={eyesBonus}
          />
          {playerScores.slice(6).map(({ cat, row }) => (
            <PlayerScoresRow
              category={cat}
              row={row}
              key={`score-table-row-${cat}`}
            />
          ))}
        </tbody>
        <tfoot className="is-size-4">
          <TotalRow totalScores={playerTotalScore} />
        </tfoot>
      </table>
    </div>
  );
};

export default observer(ScoreTable);
