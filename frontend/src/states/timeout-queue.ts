import { runInAction, makeAutoObservable } from "mobx";
export class TimeoutQueue<T> {
  queue: T[];
  ms: number;

  constructor(ms: number) {
    makeAutoObservable(this);
    this.queue = [];
    this.ms = ms;
    this.enqueue = this.enqueue.bind(this);
  }

  enqueue(elem: T) {
    this.queue.push(elem);
    setTimeout(() => {
      runInAction(() => {
        if (this.queue.length > 0) {
          this.queue.shift();
        }
      });
    }, this.ms);
  }
}
