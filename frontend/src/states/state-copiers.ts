/**
 * state-copiers.ts
 *
 * Helper functions to copy state properties.
 * NOTE: the patched state Maps are MapSchema; cloneDeep methods are not applicable.
 */
import { IGameStateProp } from "@shared/models/GameState";
import { IUserProp } from "@shared/models/User";
import { IDicesProp } from "@shared/models/Dices";

function copyDicesProp(prop: IDicesProp): IDicesProp {
  return {
    diceNum: [...prop.diceNum],
    diceFixed: [...prop.diceFixed],
    diceRolled: [...prop.diceRolled],
  };
}

function copyUserProp(prop: IUserProp): IUserProp {
  const { name, sessionId, ready } = prop;
  const scores = new Map(prop.scores.entries());
  return {
    name,
    sessionId,
    ready,
    scores,
  };
}

function copyUserPropMap(map: Map<string, IUserProp>): Map<string, IUserProp> {
  return new Map([...map.entries()].map(([k, v]) => [k, copyUserProp(v)]));
}

export function copyGameStateProp(prop: IGameStateProp): IGameStateProp {
  const {
    maxPlayers,
    ruleType,
    proceed,
    turn,
    rollCount,
    maxRollCount,
    rolling,
  } = prop;
  const players = copyUserPropMap(prop.players);
  const watchers = copyUserPropMap(prop.watchers);
  const playerOrder = [...prop.playerOrder];
  const watcherOrder = [...prop.watcherOrder];
  const dices = copyDicesProp(prop.dices);

  return {
    players,
    watchers,
    maxPlayers,
    ruleType,
    proceed,
    playerOrder,
    watcherOrder,
    turn,
    rollCount,
    maxRollCount,
    dices,
    rolling,
  };
}
