import { createContext } from "react";
import { makeAutoObservable, runInAction } from "mobx";
import * as Colyseus from "colyseus.js";

import { IGameStateProp } from "@shared/models/GameState";
import { RuleType } from "@shared/models/Rules";
import { ClientMessage } from "@shared/messages/ClientMessage";
import {
  ServerMessageType,
  isServerShowResultData,
  isServerShowStringEffectData,
  isServerShowCategoryHighlightData,
  ServerShowResultData,
  ServerShowCategoryHighlightData,
} from "@shared/messages/ServerMessage";

import { copyGameStateProp } from "./state-copiers";
import { TimeoutQueue } from "./timeout-queue";
import {
  messageDuration,
  categoryHighlightDuration,
  stringEffectDuration,
} from "../config";

const host = window.document.location.host;
const protocol = window.document.location.protocol.replace("http", "ws");

const clientURL = process.env.REACT_APP_PROXY_USED
  ? `${protocol}//${host}/ws`
  : `${protocol}//${host.replace(/:.*/, "")}:3001`;

const colyseusClient = new Colyseus.Client(clientURL);
let colyseusRoom: Colyseus.Room<IGameStateProp> | undefined = undefined;

interface RoomProp {
  sessionId: string;
  id: string;
  name: string;
}

interface MessageQueueElementOption {
  color?:
    | "is-dark"
    | "is-primary"
    | "is-link"
    | "is-info"
    | "is-success"
    | "is-warning"
    | "is-danger";
  header?: string;
  size?: "is-small" | "is-medium" | "is-large";
}

interface MessageQueueElement extends MessageQueueElementOption {
  body: string;
  key: string;
}

export class RoomStore {
  joinStatus: "init" | "pending" | "success" = "init";
  messageQueue: TimeoutQueue<MessageQueueElement> = new TimeoutQueue<MessageQueueElement>(
    messageDuration
  );

  state: IGameStateProp | undefined;
  roomProp: RoomProp | undefined;

  result: ServerShowResultData | undefined;
  visualEffect: boolean = false;

  categoryHighlightQueue: TimeoutQueue<ServerShowCategoryHighlightData> = new TimeoutQueue<ServerShowCategoryHighlightData>(
    categoryHighlightDuration
  );
  stringEffectQueue: TimeoutQueue<string> = new TimeoutQueue(
    stringEffectDuration
  );

  constructor() {
    makeAutoObservable(this);
    this.connect = this.connect.bind(this);
    this.copyState = this.copyState.bind(this);
    this.disconnect = this.disconnect.bind(this);
    this.activateResult = this.activateResult.bind(this);
    this.deactivateResult = this.deactivateResult.bind(this);
    this.activateVisualEffect = this.activateVisualEffect.bind(this);
    this.deactivateVisualEffect = this.deactivateVisualEffect.bind(this);
    this.pushMessageQueue = this.pushMessageQueue.bind(this);
    this.sendClientMessage = this.sendClientMessage.bind(this);
  }

  *connect(name: string, ruleType: RuleType) {
    this.joinStatus = "pending";

    try {
      colyseusRoom = yield colyseusClient.joinOrCreate<IGameStateProp>(
        ruleType,
        {
          name,
        }
      );
      if (colyseusRoom === undefined) {
        throw new Error("Server is not working");
      }

      const { sessionId, id, name: roomHandler } = colyseusRoom;
      this.roomProp = { sessionId, id, name: roomHandler };

      colyseusRoom.onStateChange.once((state) => {
        this.copyState(state);
      });
      colyseusRoom.onStateChange((state) => {
        this.copyState(state);
      });
      colyseusRoom.onLeave((code) => {
        runInAction(() => {
          if (colyseusRoom !== undefined) {
            this.disconnect(code);
          }
        });
      });
      colyseusRoom.onMessage("*", (message, data) => {
        runInAction(() => {
          // debug
          /*
          this.pushMessageQueue(`${message} - ${JSON.stringify(data)}`, {
            color: "is-dark",
          });*/

          switch (message) {
            case ServerMessageType.SHOW_RESULT:
              if (isServerShowResultData(data)) {
                this.activateResult(data);
              }
              break;
            case ServerMessageType.SHOW_CATEGORY_HIGHLIGHT:
              if (isServerShowCategoryHighlightData(data)) {
                this.categoryHighlightQueue.enqueue(data);
              }
              break;
            case ServerMessageType.SHOW_STRING_EFFECT:
              if (isServerShowStringEffectData(data)) {
                this.stringEffectQueue.enqueue(data.str);
              }
              break;
            case ServerMessageType.SHOW_VISUAL_EFFECT:
              this.activateVisualEffect();
              break;
            default:
              break;
          }
        });
      });

      this.joinStatus = "success";
      this.pushMessageQueue(`Accessed successfully`, {
        color: "is-success",
        header: `Welcome, ${name}`,
      });
    } catch (e) {
      console.error(e);
      this.pushMessageQueue(`${e}`, {
        color: "is-danger",
        header: "Something wrong",
      });
      this.disconnect();
    }
  }

  copyState(state: IGameStateProp) {
    this.state = copyGameStateProp(state);
  }

  disconnect(code?: number) {
    colyseusRoom?.leave();
    colyseusRoom = undefined;
    this.state = undefined;
    this.roomProp = undefined;
    this.joinStatus = "init";
    if (code === undefined || code <= 1000) {
      this.pushMessageQueue("You've just leaved the game room.");
    } else {
      this.pushMessageQueue(
        `Abnormal socket shutdown happened (Code: ${code})`,
        { color: "is-danger" }
      );
    }
  }

  activateResult(data: ServerShowResultData) {
    this.result = data;
  }

  deactivateResult() {
    this.result = undefined;
  }

  activateVisualEffect() {
    this.visualEffect = true;
  }

  deactivateVisualEffect() {
    this.visualEffect = false;
  }

  pushMessageQueue(
    body: string,
    option: MessageQueueElementOption = {
      color: undefined,
      header: undefined,
      size: undefined,
    }
  ) {
    this.messageQueue.enqueue({
      body,
      ...option,
      key: `message-queue-element-${Date.now()}-${Math.random()}`,
    });
  }

  sendClientMessage(clientMessage: ClientMessage) {
    const { message, data } = clientMessage;
    colyseusRoom?.send(message, data);
  }
}

const roomStore = createContext(new RoomStore());
export default roomStore;
