import React from "react";

import { Category } from "@shared/models/Category";
import {
  categoryShortName,
  conditionalArrayElement as CAE,
} from "@shared/helpers";

interface PlayerScoresRowProp {
  category: Category;
  row: {
    id: string;
    score: number | undefined;
    evaluated: number | undefined;
    canPostByID: boolean;
    canPostBySelf: boolean;
    onPost?: () => void;
    highlight: boolean;
  }[];
}

const PlayerScoresRow: React.FC<PlayerScoresRowProp> = ({ category, row }) => {
  return (
    <tr>
      <td className="has-text-right">{categoryShortName(category)}</td>
      {row.map(
        ({
          id,
          score,
          evaluated,
          canPostByID,
          canPostBySelf,
          onPost,
          highlight,
        }) => {
          const showAsPosted = score !== undefined;
          const showAsEvaluated =
            !showAsPosted && canPostByID && evaluated !== undefined;
          const showAsPostButton = showAsEvaluated && canPostBySelf;

          const key = `score-table-elem-${category}-${id}`;

          if (showAsPostButton) {
            return (
              <td
                key={key}
                className="has-text-primary is-italic has-text-centered"
                style={{ cursor: "pointer" }}
                onClick={onPost}
              >
                {evaluated}
              </td>
            );
          }

          const className = [
            "has-text-centered",
            ...CAE(showAsEvaluated, "has-text-grey-lighter"),
            ...CAE(showAsPosted, "has-text-weight-bold"),
            ...CAE(highlight, "has-background-primary has-text-white"),
          ].join(" ");
          return (
            <td key={key} className={className}>
              {showAsEvaluated ? evaluated : score}
            </td>
          );
        }
      )}
    </tr>
  );
};

export default PlayerScoresRow;
