import React from "react";

interface SubtotalRowProp {
  subtotalScores: { id: string; score: number }[];
  threshold?: number;
  bonus?: number;
}

const SubtotalRow: React.FC<SubtotalRowProp> = ({
  subtotalScores,
  threshold,
  bonus,
}) => {
  const className = ["has-text-right"].join(" ");
  const showBonus = (score: number) =>
    bonus !== undefined && threshold !== undefined && score >= threshold;
  return (
    <tr>
      <th>Subtotal</th>
      {subtotalScores.map(({ id, score }) => (
        <th className={className} key={`score-table-subtotal-${id}`}>
          {showBonus(score) ? (
            <span className="has-text-success">+{bonus}</span>
          ) : (
            <span>{`${score}${
              threshold !== undefined && `/${threshold}`
            }`}</span>
          )}
        </th>
      ))}
    </tr>
  );
};

export default SubtotalRow;
