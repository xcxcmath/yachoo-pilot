import React from "react";

interface PlayerNamesRowProp {
  players: { sessionId: string; name: string }[];
}

const PlayerNamesRow: React.FC<PlayerNamesRowProp> = ({ players }) => {
  return (
    <tr>
      <th></th>
      {players.map(({ sessionId, name }) => (
        <th className="has-text-centered" key={`score-table-head-${sessionId}`}>
          {name}
        </th>
      ))}
    </tr>
  );
};

export default PlayerNamesRow;
