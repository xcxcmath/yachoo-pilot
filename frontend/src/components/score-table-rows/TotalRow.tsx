import React from "react";
import _ from "lodash";

import { conditionalArrayElement as CAE } from "@shared/helpers/index";

interface TotalRowProp {
  totalScores: { id: string; score: number }[];
}
const TotalRow: React.FC<TotalRowProp> = ({ totalScores }) => {
  const maxScore = _.maxBy(totalScores, (it) => it.score)?.score;
  return (
    <tr>
      <th>Total</th>
      {totalScores.map(({ id, score }) => {
        const className = [
          "has-text-centered",
          ...CAE(score === maxScore, "has-text-success"),
        ].join(" ");
        return (
          <th key={`score-table-total-${id}`} className={className}>
            {score}
          </th>
        );
      })}
    </tr>
  );
};

export default TotalRow;
