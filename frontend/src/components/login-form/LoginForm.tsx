import "./LoginForm.scss";
import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser, faTimes } from "@fortawesome/free-solid-svg-icons";

import { RuleType, ruleMap, isRuleType } from "@shared/models/Rules";
import { maxNameLength } from "@shared/constants";

const ruleList = Object.keys(ruleMap);

interface LoginFormProp {
  checkValidSubmit?: (name: string, ruleType: RuleType) => void;
  onSubmit?: (name: string, ruleType: RuleType) => void;
  joinStatus: "init" | "pending" | "success";
}

const LoginForm: React.FC<LoginFormProp> = ({ onSubmit, joinStatus }) => {
  const init = joinStatus === "init";
  const pending = joinStatus === "pending";

  const [name, setName] = useState("");
  const [isInvalidName, setIsInvalidName] = useState(false);
  const [ruleType, setRuleType] = useState<RuleType>("default");
  const handleRuleType = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const newRuleType = e.target.value;
    if (!isRuleType(newRuleType)) return;
    setRuleType(newRuleType);
  };

  const formClassName = ["login-form"].join(" ");
  const controlClassName = (leftIcon?: boolean, rightIcon?: boolean) =>
    [
      "control",
      ...(leftIcon ? ["has-icons-left"] : []),
      ...(rightIcon ? ["has-icons-right"] : []),
      ...(pending ? ["is-loading"] : []),
    ].join(" ");
  const buttonClassName = [
    "button",
    "is-large",
    "is-primary",
    ...(pending ? ["is-loading"] : []),
  ].join(" ");
  const nameInputClassName = [
    "input",
    "is-medium",
    ...(isInvalidName ? ["is-danger"] : []),
  ].join(" ");

  return (
    <form
      className={formClassName}
      onSubmit={(e) => {
        e.preventDefault();
        onSubmit?.(name, ruleType);
      }}
    >
      <h1 className="title is-size-1">Yachoo Pilot Project</h1>
      <div className="field">
        <label className="label is-large">Name</label>
        <div className={controlClassName(true, true)}>
          <input
            className={nameInputClassName}
            type="text"
            required
            value={name}
            maxLength={maxNameLength}
            placeholder={`Up to ${maxNameLength} characters`}
            disabled={!init}
            onChange={(e) => setName(e.target.value)}
            onInvalid={(e) => {
              e.preventDefault();
              setIsInvalidName(true);
            }}
            onInput={() => {
              setIsInvalidName(false);
            }}
          />
          <span className="icon is-small is-left">
            <FontAwesomeIcon icon={faUser} />
          </span>
          {isInvalidName && (
            <span className="icon is-small is-right">
              <FontAwesomeIcon icon={faTimes} />
            </span>
          )}
        </div>
        {isInvalidName && <p className="help is-danger">Invalid name</p>}
      </div>
      <div className="field">
        <label className="label is-large">Rule</label>
        <div className={controlClassName()}>
          <div className="select">
            <select value={ruleType} onChange={handleRuleType} disabled={!init}>
              {ruleList.map((ruleName) => (
                <option key={`rule-${ruleName}`} value={ruleName}>
                  {ruleName.toUpperCase()}
                </option>
              ))}
            </select>
          </div>
        </div>
      </div>
      <button className={buttonClassName} disabled={!init}>
        Join To Yachoo
      </button>
    </form>
  );
};

export default LoginForm;
