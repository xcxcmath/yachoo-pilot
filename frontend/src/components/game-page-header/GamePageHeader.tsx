import "./GamePageHeader.scss";
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSignOutAlt } from "@fortawesome/free-solid-svg-icons";

interface GamePageHeaderProp {
  onLeaveClick?: () => void;
  isPrimary: boolean;
}

const GamePageHeader: React.FC<GamePageHeaderProp> = ({
  onLeaveClick,
  isPrimary,
}) => {
  const navClassName = [
    "game-page-header",
    "navbar",
    ...(isPrimary ? ["is-primary"] : []),
  ].join(" ");
  return (
    <nav className={navClassName}>
      <div className="navbar-brand mx-5">
        <div className="block is-size-2 has-text-weight-bold">Yachoo</div>
      </div>
      <div className="navbar-menu">
        <div className="navbar-end">
          <div className="navbar-item">
            <div className="buttons">
              <button
                className="button is-danger is-light"
                onClick={onLeaveClick}
              >
                <span>Leave Game</span>
                <span className="icon">
                  <FontAwesomeIcon icon={faSignOutAlt} />
                </span>
              </button>
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default GamePageHeader;
