import React from "react";
import { observer } from "mobx-react-lite";
import Confetti from "react-confetti";
import { useWindowSize } from "react-use";
import { conditionalArrayElement as CAE } from "@shared/helpers";

interface ResultModalProp {
  isActive: boolean;
  showConfetti: boolean;
  onClose?: () => void;
}

const ResultModal: React.FC<ResultModalProp> = ({
  children,
  isActive,
  showConfetti,
  onClose,
}) => {
  const { width, height } = useWindowSize();
  const className = [
    "result-modal",
    "modal",
    ...CAE(isActive, "is-active"),
  ].join(" ");
  return (
    <div className={className}>
      <div className="modal-background" onClick={onClose} />
      {isActive && showConfetti && <Confetti width={width} height={height} />}
      <div className="modal-content box">{children}</div>
      <button className="modal-close is-large" onClick={onClose} />
    </div>
  );
};

export default observer(ResultModal);
