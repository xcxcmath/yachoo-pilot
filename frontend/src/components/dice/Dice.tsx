import "./Dice.scss";
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faDiceOne,
  faDiceTwo,
  faDiceThree,
  faDiceFour,
  faDiceFive,
  faDiceSix,
  faQuestion,
} from "@fortawesome/free-solid-svg-icons";

import { conditionalArrayElement as CAE } from "../../../../shared/src/helpers/index";

const diceIcons = [
  faDiceOne,
  faDiceTwo,
  faDiceThree,
  faDiceFour,
  faDiceFive,
  faDiceSix,
];
function getDiceIcons(num: number) {
  switch (num) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
      return diceIcons[num - 1];
    default:
      return faQuestion;
  }
}

interface DiceProp {
  idle: boolean;
  rolling: boolean;
  type: "fixed" | "rolled";
  typeIndex: number;
  value: number;
  onClick: () => void;
}

const Dice: React.FC<DiceProp> = ({
  idle,
  rolling,
  type,
  typeIndex,
  value,
  onClick,
}) => {
  const className = [
    "dice",
    "button",
    "is-large",
    "is-bordered",
    ...CAE(idle, `dice-idle`),
    ...CAE(!idle && rolling && type === "rolled", "dice-rolling"),
    ...CAE(!idle && !rolling && type === "rolled", "dice-rolled is-primary"),
    ...CAE(!idle && type === "fixed", "dice-fixed"),
    `dice-index-${typeIndex}`,
  ].join(" ");
  return (
    <button className={className} disabled={idle || rolling} onClick={onClick}>
      <span className="icon is-medium">
        <FontAwesomeIcon
          className="fa-2x"
          icon={
            idle || (rolling && type === "rolled")
              ? faQuestion
              : getDiceIcons(value)
          }
        />
      </span>
    </button>
  );
};

export default Dice;
