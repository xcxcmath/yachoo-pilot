import "./UserList.scss";
import React from "react";

import UserListCell from "./UserListCell";

interface UserListProp {
  heading: React.ReactNode;
  primary?: boolean;
  description?: string;
  children?: React.ReactElement<typeof UserListCell>[];
}

const UserList: React.FC<UserListProp> = ({
  heading,
  primary,
  description,
  children,
}) => {
  const isEmpty = children === undefined || children.length === 0;
  const className = [
    "user-list",
    "has-text-centered",
    "panel",
    ...(primary ? ["is-primary"] : []),
  ].join(" ");
  return (
    <div className={className}>
      <p className="panel-heading">{heading}</p>
      {description && (
        <div className="has-text-grey">
          <p className="is-size-7">{description}</p>
        </div>
      )}
      {isEmpty && <h1>List is empty</h1>}
      {children}
    </div>
  );
};

export default UserList;
