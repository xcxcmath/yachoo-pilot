import "./UserListCell.scss";
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser } from "@fortawesome/free-solid-svg-icons";

interface UserListCellProp {
  active?: boolean;
  name: string;
  isYou?: boolean;
  isLeader?: boolean;
  isReady?: boolean;
}

const UserListCell: React.FC<UserListCellProp> = ({
  active,
  name,
  isYou,
  isLeader,
  isReady,
}) => {
  const className = [
    "user-list-cell",
    "panel-block",
    ...(active ? ["is-active"] : []),
  ].join(" ");
  return (
    <div className={className}>
      <span className="panel-icon">
        <FontAwesomeIcon icon={faUser} />
      </span>
      <h1 className={active ? "is-size-3 has-text-primary" : "is-size-5"}>
        {name}
      </h1>
      <span className="tags">
        {isYou && <span className="tag is-primary is-medium">You</span>}
        {isLeader && <span className="tag is-success is-medium">Leader</span>}
        {isReady && (
          <span className="tag is-success is-light is-medium">Ready</span>
        )}
      </span>
    </div>
  );
};

export default UserListCell;
