import "./AlertModal.scss";
import React from "react";

interface AlertModalProp {
  isActive: boolean;
  onConfirm?: () => void;
  onCancel?: () => void;
}

const AlertModal: React.FC<AlertModalProp> = ({
  children,
  isActive,
  onConfirm,
  onCancel,
}) => {
  const className = [
    "alert-modal",
    "modal",
    ...(isActive ? ["is-active"] : []),
  ].join(" ");
  return (
    <div className={className}>
      <div className="modal-background"></div>
      <div className="modal-content message is-danger">
        <div className="message-header">
          <p>Warning</p>
        </div>
        <div className="message-body">
          <div className="block has-text-centered">{children}</div>
          <div className="block buttons is-centered">
            <div
              className="button is-primary is-medium has-text-weight-bold"
              onClick={onConfirm}
            >
              Yes
            </div>
            <div className="button is-medium" onClick={onCancel}>
              No
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AlertModal;
