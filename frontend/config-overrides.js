const {
  removeModuleScopePlugin,
  override,
  babelInclude,
  addWebpackAlias,
  addWebpackModuleRule,
} = require("customize-cra");
const path = require("path");

module.exports = override(
  addWebpackAlias({ "@shared": path.resolve(__dirname, "../shared/src") }),
  addWebpackModuleRule({
    test: /\.s[ac]ss$/i,
    use: [
      "style-loader",
      "css-loader",
      {
        loader: "sass-loader",
        options: {
          implementation: require("sass"),
        },
      },
    ],
  }),
  removeModuleScopePlugin(),
  babelInclude([path.resolve("src"), path.resolve("../shared/src")])
);
