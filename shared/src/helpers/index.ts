import { Category } from "../models/Category";

export const categoryShortName = (cat: Category) => {
  switch (cat) {
    case Category.ACES:
      return "Aces";
    case Category.DEUCES:
      return "Deuces";
    case Category.THREES:
      return "Threes";
    case Category.FOURS:
      return "Fours";
    case Category.FIVES:
      return "Fives";
    case Category.SIXES:
      return "Sixes";
    case Category.CHOICE:
      return "Choice";
    case Category.FOURKIND:
      return "4 of a Kind";
    case Category.FULLHOUSE:
      return "Full House";
    case Category.SMALLSTRAIGHT:
      return "S. Straight";
    case Category.LARGESTRAIGHT:
      return "L. Straight";
    case Category.YACHOO:
      return "YACHOO";
    default:
      return "(unknown)";
  }
};
export const categoryLongName = (cat: Category) => {
  switch (cat) {
    case Category.ACES:
      return "Aces";
    case Category.DEUCES:
      return "Deuces";
    case Category.THREES:
      return "Threes";
    case Category.FOURS:
      return "Fours";
    case Category.FIVES:
      return "Fives";
    case Category.SIXES:
      return "Sixes";
    case Category.CHOICE:
      return "Choice";
    case Category.FOURKIND:
      return "4 of a Kind";
    case Category.FULLHOUSE:
      return "Full House";
    case Category.SMALLSTRAIGHT:
      return "Small Straight";
    case Category.LARGESTRAIGHT:
      return "Large Straight";
    case Category.YACHOO:
      return "YACHOO";
    default:
      return "(Unknown category)";
  }
};
export const randomIntInclusive = (min: number, max: number) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

export const conditionalArrayElement = <T>(
  condition: boolean,
  value: T
): T[] => {
  return condition ? [value] : [];
};
