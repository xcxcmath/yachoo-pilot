import { Category } from "../models/Category";

export enum ClientMessageType {
  TOGGLE_READY = "toggle-ready",
  TOGGLE_DICE = "toggle-dice",
  ROLL = "roll",
  POST_CATEGORY = "post-category",
}

export interface ClientToggleReadyMessage {
  message: ClientMessageType.TOGGLE_READY;
  data: {};
}

export interface ClientToggleDiceData {
  index: number;
}
export function isClientToggleDiceData(obj: any): obj is ClientToggleDiceData {
  const { index } = obj;
  return index !== undefined && typeof index === "number";
}
export interface ClientToggleDiceMessage {
  message: ClientMessageType.TOGGLE_DICE;
  data: ClientToggleDiceData;
}

export interface ClientRollMessage {
  message: ClientMessageType.ROLL;
  data: {};
}

export interface ClientPostCategoryData {
  category: Category;
}
export function isClientPostCategoryData(
  obj: any
): obj is ClientPostCategoryData {
  const { category } = obj;
  return category !== undefined && typeof category === "string";
}
export interface ClientPostCategoryMessage {
  message: ClientMessageType.POST_CATEGORY;
  data: ClientPostCategoryData;
}

export type ClientMessage =
  | ClientToggleReadyMessage
  | ClientToggleDiceMessage
  | ClientRollMessage
  | ClientPostCategoryMessage;
