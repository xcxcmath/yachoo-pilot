export enum ServerMessageType {
  SHOW_RESULT = "show-result",
  SHOW_STRING_EFFECT = "show-string-effect",
  SHOW_VISUAL_EFFECT = "show-visual-effect",
  SHOW_CATEGORY_HIGHLIGHT = "show-category-highlight",
}

export interface ServerShowResultData {
  sessionIDs: string[];
  scores: number[];
  names: string[];
}
export function isServerShowResultData(obj: any): obj is ServerShowResultData {
  try {
    const validSessionIDs =
      Array.isArray(obj.sessionIDs) &&
      obj.sessionIDs.every((id: any) => typeof id === "string");
    const validScores =
      Array.isArray(obj.scores) &&
      obj.scores.every((score: any) => typeof score === "number");
    const validNames =
      Array.isArray(obj.names) &&
      obj.names.every((name: any) => typeof name === "string");
    return (
      validSessionIDs &&
      validScores &&
      validNames &&
      obj.sessionIDs.length === obj.scores.length &&
      obj.scores.length === obj.names.length
    );
  } catch (e) {
    return false;
  }
}
export interface ServerShowResultMessage {
  message: ServerMessageType.SHOW_RESULT;
  data: ServerShowResultData;
}

export interface ServerShowStringEffectData {
  str: string;
}
export function isServerShowStringEffectData(
  obj: any
): obj is ServerShowStringEffectData {
  return obj.str !== undefined && typeof obj.str === "string";
}
export interface ServerShowStringEffectMessage {
  message: ServerMessageType.SHOW_STRING_EFFECT;
  data: ServerShowStringEffectData;
}

export interface ServerShowVisualEffectMessage {
  message: ServerMessageType.SHOW_VISUAL_EFFECT;
  data: {};
}

export interface ServerShowCategoryHighlightData {
  sessionId: string;
  category: string;
}
export function isServerShowCategoryHighlightData(
  obj: any
): obj is ServerShowCategoryHighlightData {
  const validSessionId =
    obj.sessionId !== undefined && typeof obj.sessionId === "string";
  const validCategory =
    obj.category !== undefined && typeof obj.category === "string";
  return validSessionId && validCategory;
}
export interface ServerShowCategoryHighlightMessage {
  message: ServerMessageType.SHOW_CATEGORY_HIGHLIGHT;
  data: ServerShowCategoryHighlightData;
}

export type ServerMessage =
  | ServerShowResultMessage
  | ServerShowStringEffectMessage
  | ServerShowVisualEffectMessage
  | ServerShowCategoryHighlightMessage;
