import _ from "lodash";
import {
  Category,
  defaultCategoriesList,
  eyesCategoriesList,
} from "./Category";
export type ScoresType = Map<string, number>;

export interface IScoreCalculator {
  eyesBonusThreshold: number;
  eyesBonus: number;
  getTotalScore(scores: ScoresType): number;

  getEyesCategoriesScore(scores: ScoresType): number;

  postCategory(
    scores: ScoresType,
    cat: Category,
    diceNum: Array<number>
  ): number | undefined;

  calcCategoryScore(
    scores: ScoresType,
    cat: Category,
    diceNum: Array<number>
  ): number | undefined;

  isFinished(scores: ScoresType): boolean;
}

export class DefaultScoreCalculator implements IScoreCalculator {
  eyesBonusThreshold: number = 63;
  eyesBonus: number = 35;

  constructor() {
    this.getTotalScore = this.getTotalScore.bind(this);
    this.getEyesCategoriesScore = this.getEyesCategoriesScore.bind(this);
    this.postCategory = this.postCategory.bind(this);
    this.calcCategoryScore = this.calcCategoryScore.bind(this);
    this.isFinished = this.isFinished.bind(this);
  }

  getTotalScore(scores: ScoresType) {
    return (
      _.sumBy(defaultCategoriesList, (cat) => scores.get(cat) ?? 0) +
      (this.getEyesCategoriesScore(scores) >= this.eyesBonusThreshold
        ? this.eyesBonus
        : 0)
    );
  }

  getEyesCategoriesScore(scores: ScoresType) {
    return _.chain(eyesCategoriesList)
      .sumBy((cat) => scores.get(cat) ?? 0)
      .value();
  }

  postCategory(scores: ScoresType, cat: Category, diceNum: Array<number>) {
    const result = this.calcCategoryScore(scores, cat, diceNum);
    if (result !== undefined) {
      scores.set(cat, result);
    }
    return result;
  }

  calcCategoryScore(
    scores: ScoresType,
    cat: Category,
    diceNum: Array<number>
  ): number | undefined {
    const alreadyPosted = scores.has(cat);
    if (alreadyPosted) return undefined;

    const occurrencesSorted = _.chain(diceNum)
      .countBy()
      .values()
      .sortBy()
      .value();
    const sum = _.sumBy(diceNum);

    const eyesScore = (n: number) =>
      diceNum.filter((it) => it === n).length * n;

    const isOccurrencesArrayIn = (targets: Array<number>[]) => {
      return targets.some((arr) => _.isEqual(occurrencesSorted, arr));
    };

    const maxCons = _.chain(diceNum)
      .sortBy()
      .uniq()
      .reduce(
        (acc, here) => {
          const consecutive = here === acc.lastNum + 1;
          const curCons = consecutive ? acc.curCons + 1 : 1;
          const maxCons = Math.max(curCons, acc.maxCons);
          return { maxCons, curCons, lastNum: here };
        },
        { maxCons: 0, curCons: 0, lastNum: -9999 }
      )
      .value().maxCons;

    switch (cat) {
      case Category.ACES:
        return eyesScore(1);
      case Category.DEUCES:
        return eyesScore(2);
      case Category.THREES:
        return eyesScore(3);
      case Category.FOURS:
        return eyesScore(4);
      case Category.FIVES:
        return eyesScore(5);
      case Category.SIXES:
        return eyesScore(6);
      case Category.CHOICE:
        return sum;
      case Category.FOURKIND:
        return isOccurrencesArrayIn([[5], [1, 4]]) ? sum : 0;
      case Category.FULLHOUSE:
        return isOccurrencesArrayIn([[5], [2, 3]]) ? sum : 0;
      case Category.SMALLSTRAIGHT:
        return maxCons >= 4 ? 15 : 0;
      case Category.LARGESTRAIGHT:
        return maxCons >= 5 ? 30 : 0;
      case Category.YACHOO:
        return _.uniq(diceNum).length === 1 ? 50 : 0;

      default:
        return undefined;
    }
  }

  isFinished(scores: ScoresType) {
    return scores.size >= defaultCategoriesList.length;
  }
}
