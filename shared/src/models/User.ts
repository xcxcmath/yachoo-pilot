import { ScoresType } from "./ScoreCalculator";
export interface IUserProp {
  name: string;
  sessionId: string;
  ready: boolean;
  scores: ScoresType;
}
