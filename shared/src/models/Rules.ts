import { DefaultScoreCalculator, IScoreCalculator } from "./ScoreCalculator";
import { DefaultGameStateChecker, IGameStateChecker } from "./GameStateChecker";
import { Category, defaultCategoriesList } from "./Category";

export type RuleHelper = {
  scoreCalculator: IScoreCalculator;
  stateChecker: IGameStateChecker;
  categories: Category[];
  stringShowCategories: Category[]; // priorities are in descending order for same scores if any
  visualShowCategories: Category[];
};
interface RuleMap {
  default: RuleHelper;
}

export const ruleMap: RuleMap = {
  default: {
    scoreCalculator: new DefaultScoreCalculator(),
    stateChecker: new DefaultGameStateChecker(),
    categories: defaultCategoriesList,
    stringShowCategories: [
      Category.YACHOO,
      Category.LARGESTRAIGHT,
      Category.SMALLSTRAIGHT,
      Category.FULLHOUSE,
      Category.FOURKIND,
    ],
    visualShowCategories: [Category.YACHOO],
  },
};

export type RuleType = keyof typeof ruleMap;
export function isRuleType(obj: any): obj is RuleType {
  return Object.keys(ruleMap).some((s) => s === obj);
}
