export interface IDicesProp {
  diceNum: Array<number>;
  diceFixed: Array<number>;
  diceRolled: Array<number>;
}
