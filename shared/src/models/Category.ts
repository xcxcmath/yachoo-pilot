export enum Category {
  ACES = "aces",
  DEUCES = "deuces",
  THREES = "threes",
  FOURS = "fours",
  FIVES = "fives",
  SIXES = "sixes",

  CHOICE = "choice",
  FOURKIND = "four-kind",
  FULLHOUSE = "full-house",
  SMALLSTRAIGHT = "small-straight",
  LARGESTRAIGHT = "large-straight",
  YACHOO = "yachoo",
}

export const eyesCategoriesList = [
  Category.ACES,
  Category.DEUCES,
  Category.THREES,
  Category.FOURS,
  Category.FIVES,
  Category.SIXES,
];

export const defaultCategoriesList = [
  ...eyesCategoriesList,
  Category.CHOICE,
  Category.FOURKIND,
  Category.FULLHOUSE,
  Category.SMALLSTRAIGHT,
  Category.LARGESTRAIGHT,
  Category.YACHOO,
];
