import { IGameStateProp } from "./GameState";

export interface IGameStateChecker {
  isPlayersTurn(state: IGameStateProp, sessionId: string): boolean;
  isPlayerControllable(state: IGameStateProp, sessionId: string): boolean;
  isStartable(state: IGameStateProp): boolean;
  isIdle(state: IGameStateProp): boolean;

  isPlayerReadyToggleable(state: IGameStateProp, sessionId: string): boolean;
  isPlayerDiceToggleable(state: IGameStateProp, sessionId: string): boolean;
  isPlayerRollable(state: IGameStateProp, sessionId: string): boolean;
  isPlayerPostable(state: IGameStateProp, sessionId: string): boolean;
}

export class DefaultGameStateChecker implements IGameStateChecker {
  constructor() {
    this.isPlayersTurn = this.isPlayersTurn.bind(this);
    this.isPlayerControllable = this.isPlayerControllable.bind(this);
    this.isStartable = this.isStartable.bind(this);
    this.isIdle = this.isIdle.bind(this);

    this.isPlayerReadyToggleable = this.isPlayerReadyToggleable.bind(this);
    this.isPlayerDiceToggleable = this.isPlayerDiceToggleable.bind(this);
    this.isPlayerRollable = this.isPlayerRollable.bind(this);
    this.isPlayerPostable = this.isPlayerPostable.bind(this);
  }

  /**
   * Predicate checking whether the client command is from valid user
   * @param sessionId
   */
  isPlayersTurn(state: IGameStateProp, sessionId: string): boolean {
    return state.proceed && state.playerOrder[state.turn] === sessionId;
  }

  /**
   * Predicate checking whether the client command is from valid user on valid time
   * @param state
   */
  isPlayerControllable(state: IGameStateProp, sessionId: string): boolean {
    return this.isPlayersTurn(state, sessionId) && !state.rolling;
  }

  /**
   * Predicate checking whether the game is initiable
   */
  isStartable(state: IGameStateProp): boolean {
    return (
      !state.proceed &&
      state.players.size >= 2 &&
      [...state.players.values()].every((player) => player.ready)
    );
  }

  /**
   * Predicate checking whether dices are not rolled yet
   * @param state
   */
  isIdle(state: IGameStateProp): boolean {
    return (
      state.proceed && state.rollCount >= state.maxRollCount && !state.rolling
    );
  }

  isPlayerReadyToggleable(state: IGameStateProp, sessionId: string): boolean {
    return (
      state.players.has(sessionId) &&
      !state.watchers.has(sessionId) &&
      !state.proceed
    );
  }

  isPlayerDiceToggleable(state: IGameStateProp, sessionId: string): boolean {
    return (
      this.isPlayerControllable(state, sessionId) &&
      state.rollCount < state.maxRollCount
    );
  }

  isPlayerRollable(state: IGameStateProp, sessionId: string): boolean {
    return (
      this.isPlayerControllable(state, sessionId) &&
      state.rollCount > 0 &&
      state.dices.diceRolled.length > 0
    );
  }

  isPlayerPostable(state: IGameStateProp, sessionId: string): boolean {
    return (
      this.isPlayerControllable(state, sessionId) &&
      state.rollCount < state.maxRollCount
    );
  }
}
