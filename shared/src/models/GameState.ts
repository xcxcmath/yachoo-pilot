import { IUserProp } from "./User";
import { RuleType } from "./Rules";
import { IDicesProp } from "./Dices";

export interface IGameStateProp {
  players: Map<string, IUserProp>;
  watchers: Map<string, IUserProp>;
  maxPlayers: number;
  ruleType: RuleType;

  proceed: boolean;
  playerOrder: Array<string>; // sessionId
  watcherOrder: Array<string>;
  turn: number;

  rollCount: number;
  maxRollCount: number;

  dices: IDicesProp;

  rolling: boolean;
}
